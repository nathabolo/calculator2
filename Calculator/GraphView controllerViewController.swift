//
//  GraphView controllerViewController.swift
//  Calculator
//
//  Created by Temp on 2017/09/01.
//  Copyright © 2017 Temp. All rights reserved.
//

import UIKit
import AssetsLibrary

class GraphView_controllerViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var graphGesture: GraphView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBAction func saveCharty(_ sender: UIBarButtonItem) {
        //Variables for adding the image path
        print("Save Screenshot")
        func captureScreenshot(){
            let layer = UIApplication.shared.keyWindow!.layer
            let scale = UIScreen.main.scale
            // Creates UIImage of same size as view
            UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
            layer.render(in: UIGraphicsGetCurrentContext()!)
            let screenshot = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            // Save screen shots to the emulator's photos
            UIImageWriteToSavedPhotosAlbum(screenshot!, nil, nil, nil)
        }
    }
    
    @IBAction func switchToBarChart(_ sender: UISwitch) {
        if (sender.isEnabled == true) {
        } else {
            sender.isEnabled = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.minimumZoomScale = 0.0
        self.scrollView.maximumZoomScale = 30.0
        addPanGesture(view: graphGesture)
    }
    
    func addPanGesture(view: GraphView) {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(GraphView_controllerViewController.handlePan(sender:)))
        view.addGestureRecognizer(pan)
    }
    
    @objc func handlePan(sender: UIPanGestureRecognizer) {
        let  fileView = sender.view
        let translation = sender.translation(in: view
        )
        switch sender.state {
        case .began, .changed:
            fileView?.center = CGPoint(x: (fileView?.center.x)! + translation.x, y: (fileView?.center.y)! + translation.y)
            sender.setTranslation(CGPoint.zero, in: view)
        default:
            break
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.graphGesture
    }
}
