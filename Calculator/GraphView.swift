//
//  GraphView.swift
//  Calculator
//
//  Created by Nathanoiel on 2017/08/30.
//  Copyright © 2017 ABSA. All rights reserved.
//

import UIKit


@IBDesignable
class GraphView: UIView {
    
    public var graphvalue: CGFloat?
    var yForX: ((Double) -> Double?)? {
        didSet {
            setNeedsDisplay()
            
        }
    }
    
    
    @IBInspectable
    var scale: CGFloat = 50.0 { didSet { setNeedsDisplay() } }
    @IBInspectable
    var lineWidth: CGFloat = 2.0 { didSet { setNeedsDisplay() } }
    @IBInspectable
    var color: UIColor = UIColor.blue { didSet { setNeedsDisplay() } }
    @IBInspectable
    var colorAxes: UIColor = UIColor.black { didSet { setNeedsDisplay() } }
    
    var originRelativeToCenter = CGPoint.zero  { didSet { setNeedsDisplay() } }
    
    private var graphCenter: CGPoint {
        return convert(center, from: superview)
    }
    private  var origin: CGPoint  {
        get {
            var origin = originRelativeToCenter
            origin.x += graphCenter.x
            origin.y += graphCenter.y
            return origin
        }
        set {
            var origin = newValue
            origin.x -= graphCenter.x
            origin.y -= graphCenter.y
            originRelativeToCenter = origin
        }
    }
    
    private var axesDrawer = AxesDrawer()
    override func draw(_ rect: CGRect) {
        axesDrawer.contentScaleFactor = contentScaleFactor
        axesDrawer.color = colorAxes
        axesDrawer.drawAxes(in: bounds, origin: origin, pointsPerUnit: scale)
        drawCurveInRect(bounds, origin: origin, scale: scale)
        displayGraph()
    }
    
    
    
    func displayGraph(){
//        _ = CGRect(
//                    origin: CGPoint(x: 1, y: 7),
//                    size: UIScreen.main.bounds.size)
        let defaults = UserDefaults.standard
        //defaults.removeObject(forKey: "graphValue")
        if let getGraphValue = defaults.string(forKey: "graphOutputs")
        {
            print(getGraphValue)
            let x: Double = Double(getGraphValue)!
            graphvalue =  CGFloat(x)
        }
        
        if nil != graphvalue {
            
            print(graphvalue!)
            drawSinWave()
          //drawTheline(rect: CGPath)
        }
        
    }
    
    
    
    func drawTheline(rect: CGRect) {
        let aPath = UIBezierPath()
        aPath.move(to: CGPoint(x: bounds.width , y: bounds.height  * ((42 - (graphvalue)!)/84)))
        aPath.addLine(to: CGPoint(x: 0 , y: bounds.height  * ((42 - (graphvalue)!)/84)))
        aPath.close()
        UIColor.red.set()
        aPath.stroke()
        aPath.fill()
    }
    
    func drawSinWave(){
        let centerY = bounds.height  * ((42 - (graphvalue)!)/84)
        let steps = 315
        let stepX = frame.width / CGFloat(steps)
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: bounds.height  * ((42 - (graphvalue)!)/84)))
        for i in 0...steps {
            let x = CGFloat(i) * stepX
            let y = (sin(Double(i) * 0.1) * 20) + Double(centerY)
            path.addLine(to: CGPoint(x: x, y: CGFloat(y)))
        }
        let strokeColor = UIColor.red
        strokeColor.setStroke()
        path.lineWidth = 4
        path.stroke()
        
    }
    
    
    
    func drawCurveInRect(_ bounds: CGRect, origin: CGPoint, scale: CGFloat){
        var xGraph, yGraph :CGFloat
        var x, y: Double
        var isFirstPoint = true
        var oldYGraph: CGFloat =  0.0
        var disContinuity:Bool {
            return abs( yGraph - oldYGraph) >
                max(bounds.width, bounds.height) * 1.5}
        if yForX != nil {
            color.set()
            let path = UIBezierPath()
            path.lineWidth = lineWidth
            
            for i in 0...Int(bounds.size.width * contentScaleFactor){
                xGraph = CGFloat(i) / contentScaleFactor
                
                x = Double ((xGraph - origin.x) / scale)
                guard let y = (yForX)!(x),
                    y.isFinite else {continue}
                
                yGraph = origin.y - CGFloat(y) * scale
                
                if isFirstPoint{
                    path.move(to: CGPoint(x: xGraph, y: yGraph))
                    isFirstPoint = false
                } else {
                    if disContinuity {
                        isFirstPoint = true
                    } else {
                        path.addLine(to: CGPoint(x: xGraph, y: yGraph))
                    }
                }
            }
            path.stroke()
        }
    }
    
    
    
    
}



