//  ViewController.swift
//  Calculator
//
//  Created by Nathaniel on 2017/08/22.
//  Copyright © 2017 Temp. All rights reserved.


import UIKit

class CalculatorViewController: UIViewController {
    
    @IBOutlet weak var labelDisplay: UILabel!
    @IBOutlet weak var display: UILabel!
    
    var typingInProgress = false
    private var brain = CalculatorBrain()
    private let decimalSeparator = NumberFormatter().decimalSeparator!
    
    @IBAction func showthegraph(_ sender: UIButton) {
        let  outputValue = UserDefaults.standard
        outputValue.set(display.text!, forKey: "graphOutputs")
        print(outputValue)
        self.performSegue(withIdentifier: "a", sender: self)
    }
    
    @IBAction func touchDIgit(_ sender: UIButton) {
        var digit = sender.currentTitle!
        if(digit == "M") {
            digit = brain.Memory
        } else if (digit.contains("→M")) {
            digit = ""
        }
        
        if typingInProgress {
            if (digit == ".") && (display.text!.range(of: ".") != nil) { return }
            display.text = display.text! + digit
        } else {
            if digit == "." {
                display.text = "0" + decimalSeparator
            } else {
                display.text = digit
            }
            typingInProgress = true
        }
    }
    var displayValue: Double {
        get {
            if let text = display.text, let value = Double(text){
                return value
            }
            return 0
        }
        set {
            display.text = String(describing: newValue)
            if let description = brain.description {
                labelDisplay.text = description + (brain.resultIsPending ? " …" : " =")
            }
        }
    }
    
    @IBAction func saveToMemory(_ sender: Any) {
        brain.Memory = display.text!
        print("A: " + brain.Memory)
    }
    
    @IBAction func performOperation(_ sender: UIButton) {
        if typingInProgress {
            brain.setOperand(displayValue, variable: "named")
            typingInProgress = false
            labelDisplay.text = display.text! + " " + sender.currentTitle!
        }
        
        if let mathematicalSymbol = sender.currentTitle{
            brain.performOperation(mathematicalSymbol)
        }
        if let result = brain.result{
            displayValue = result
        }
        
        if let description = brain.description {
            labelDisplay.text = description.description + (brain.resultIsPending ? "…" : "=")
        } else {
            labelDisplay.text = " "
        }
    }
    
    //Set user typing inputs to zero/rerset user inputs
    @IBAction func resetButton(_ sender: UIButton) {
        brain = CalculatorBrain()
        displayValue = 0
        display.text = "0"
        labelDisplay.text = " "
        typingInProgress = false
    }
    
    @IBAction func backSpace(_ sender: Any) {
        if typingInProgress, var text = display.text {
            text.remove(at: text.index(before: text.endIndex))
            if text.isEmpty {
                text = "0"
                typingInProgress = false
            }
            display.text = text
            labelDisplay.text = text
        }
    }
    
    private func adjustButtonLayout(for view: UIView, isPortrait: Bool) {
        for subview in view.subviews {
            if subview.tag == 1 {
                subview.isHidden = isPortrait
            } else if subview.tag == 2 {
                subview.isHidden = !isPortrait
            }
            if let stack = subview as? UIStackView {
                adjustButtonLayout(for: stack, isPortrait: isPortrait);
            }
        }
    }
}


